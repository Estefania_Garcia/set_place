//Importar la clase express

import express from 'express';
import set_place from './set_place.js';

//Crear un objeto express
const app=express();

// Crear una ruta

app.get("/bienvenida", (peticion, respuesta)=>{

    respuesta.send("Bienvenido al mundo de Backend Node.");
});
app.get("/set place", (peticion, respuesta)=>{
    set_place();
    respuesta.send("Algo paso!!")
   
});

//Inicializar el servidor node

app.listen(3006,()=>{
    console.log("Esta funcionando el servidor."
    )});